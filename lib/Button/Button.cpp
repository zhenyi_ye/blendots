//
// Created by Zhenyi Ye on 2019-08-07.
//

#include "Button.h"

Button::Button(uint8_t pin) {
    pinNum = pin;
}

void Button::begin() {
    pinMode(pinNum, INPUT);
}

uint8_t Button::getPin() {
    return pinNum;
}

void Button::trigger() {
    char buf[15];
    snprintf(buf, 15, "btn %d !", pinNum);
    Serial.println(buf);

}