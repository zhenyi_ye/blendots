//
// Created by Zhenyi Ye on 2019-08-07.
//

#ifndef SMARTBUTTON_BUTTON_H
#define SMARTBUTTON_BUTTON_H

#include <Arduino.h>


class Button {


    public:
        Button(uint8_t pin);

        void begin();
        uint8_t getPin();
        virtual void trigger();

    protected:
        uint8_t pinNum;

};


#endif //SMARTBUTTON_BUTTON_H
