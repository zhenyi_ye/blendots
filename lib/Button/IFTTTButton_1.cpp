//
// Created by Zhenyi Ye on 2019-08-22.
//

#include "IFTTTButton_1.h"

void IFTTTButton_1::trigger() {
    char buf[2];
    snprintf(buf, sizeof(buf), "%d", 1);
    Serial.print("btn 4: ");
    Serial.println(buf);
    wh.trigger(buf);
}