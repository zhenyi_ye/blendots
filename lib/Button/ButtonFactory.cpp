//
// Created by Zhenyi Ye on 2019-08-07.
//

#include "ButtonFactory.h"

extern void wifi_reconnect();

ButtonFactory::ButtonFactory() {
    pinMode(LED_CLK, OUTPUT);
    pinMode(LED_DATA, OUTPUT);
    pinMode(LED_LATCH, OUTPUT);
    digitalWrite(LED_LATCH, LOW);
    shiftOut(LED_DATA, LED_CLK, MSBFIRST, 0);
    digitalWrite(LED_LATCH, HIGH);
}

int ButtonFactory::hash(uint8_t num_1, uint8_t num_2){
    if (num_1 > num_2)
        return int(num_2)*100 + num_1;
    else
        return int(num_1)*100 + num_2;
}

boolean ButtonFactory::loop() {

    uint8_t button_status[20];
    uint8_t active_btn_count = 0;
    uint8_t largest_active_button = 0;

    uint8_t LED_data = 0;

    for(int i=0; i<button_registry.len; i++){
        if (!digitalRead(button_registry.button_list[i]->getPin())){
            LED_data = LED_data | (1 << (i+1)); //adjusting i for 0 th element
            largest_active_button = i;
            button_status[active_btn_count++] = button_registry.button_list[i]->getPin();
        }
        else{
            LED_data = LED_data | (0 << i);
        }
    }
    // Start to read buttons
    boolean reading_btns = true;
    boolean repeated_btn = false;

    while(reading_btns){

        reading_btns = false;

        // read all buttons
        for(int i=0; i<button_registry.len; i++){
            if (!digitalRead(button_registry.button_list[i]->getPin())){

                reading_btns = true;    // continue to read as long as some buttons are pressed

                // check whether the button has been recorded
                for (int j=0; j<active_btn_count; j++)
                    if (button_registry.button_list[i]->getPin() == button_status[j]){
                        repeated_btn = true;
                        break;
                    }
                if (repeated_btn){
                    repeated_btn = !repeated_btn;
                    continue;
                }

                // update button status
                LED_data = LED_data | (1 << (i+1));
                largest_active_button = i;
                button_status[active_btn_count++] = button_registry.button_list[i]->getPin();
            }
            else
                LED_data = LED_data | (0 << (i+1));
        }

        // light up leds
        pinMode(LED_CLK, OUTPUT);
        pinMode(LED_DATA, OUTPUT);
        pinMode(LED_LATCH, OUTPUT);
        digitalWrite(LED_LATCH, LOW);
        shiftOut(LED_DATA, LED_CLK, MSBFIRST, LED_data);
        digitalWrite(LED_LATCH, HIGH);
        Serial.println(active_btn_count);
    }

    boolean ret_val = true;
    if(active_btn_count > 1)
        execute_combo(button_status, active_btn_count);
    else if(active_btn_count == 1) {
        if (WiFi.status() != WL_CONNECTED)
            wifi_reconnect();
        execute_single(largest_active_button);
    }
    else
        ret_val = false;
    // Update LEDs
    digitalWrite(LED_LATCH, LOW);
    shiftOut(LED_DATA, LED_CLK, MSBFIRST, 0);
    digitalWrite(LED_LATCH, HIGH);

    return ret_val;
}

void ButtonFactory::execute() {

}

void ButtonFactory::execute_combo(uint8_t* button_status, uint8_t acitve_btn_count) {
    for(uint8_t i=0; i<acitve_btn_count-1; i++){
        for(uint8_t j=i+1; j<acitve_btn_count; j++){
            int hashed_val = hash(*(button_status+i), *(button_status+j));
            for(uint8_t k=0; k<button_registry.combo_len; k++){
                if (hashed_val == button_registry.combo_list[k])
                    (*button_registry.combo_func[k])();
                    return;
            }
        }
    }
}

void ButtonFactory::execute_single(uint8_t largest_active_button) {
    button_registry.button_list[largest_active_button]->trigger();
}


void ButtonFactory::buttonRegister(Button *btn) {
    uint8_t pin = (*btn).getPin();
    int i = button_registry.len-1;
    while(i >= 0 && pin < button_registry.button_list[i]->getPin()){
        Serial.println(i);
        button_registry.button_list[i + 1] = button_registry.button_list[i];
        i--;
    }
    button_registry.button_list[i+1] = btn;
    button_registry.len++;
}


void ButtonFactory::btnComboRegister(Button *btn_1, Button *btn_2, void (*callback)()) {
    uint8_t pin_1 = btn_1->getPin();
    uint8_t pin_2 = btn_2->getPin();
    int hash_index = hash(pin_1, pin_2);
    button_registry.combo_list[button_registry.combo_len] = hash_index;
    button_registry.combo_func[button_registry.combo_len++] = callback;
}

