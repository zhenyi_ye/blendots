//
// Created by Zhenyi Ye on 2019-08-07.
//

#ifndef SMARTBUTTON_BUTTONFACTORY_H
#define SMARTBUTTON_BUTTONFACTORY_H

#include <Arduino.h>
#include <vector>
#include <ESP8266WiFi.h>
#include "constants.h"
#include "Button.h"

struct ButtonList{
    uint8_t len;
    Button *button_list[30];
    uint8_t combo_len;
    int combo_list[10];
    void (*combo_func[10])(void);
};


class ButtonFactory {

    private:

        ButtonList button_registry;

        static int hash(uint8_t num_1, uint8_t num_2);

        void execute_combo(uint8_t* button_status, uint8_t acitve_btn_count);
        void execute_single(uint8_t largest_active_button);

    public:

        ButtonFactory();

        boolean loop();
        void execute();

        void buttonRegister(Button* btn);
        void btnComboRegister(Button* btn_1, Button* btn_2, void (*callback)());

};


#endif //SMARTBUTTON_BUTTONFACTORY_H
