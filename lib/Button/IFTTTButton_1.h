//
// Created by Zhenyi Ye on 2019-08-22.
//

#ifndef SMARTBUTTON_IFTTTBUTTON_1_H
#define SMARTBUTTON_IFTTTBUTTON_1_H

#include <Arduino.h>
#include "Button.h"
#include "../IFTTTWebhook/IFTTTWebhook.h"

#define IFTTT_EVENT_NAME "open_web"
#define IFTTT_FINGERPRINT "AA:75:CB:41:2E:D5:F9:97:FF:5D:A0:8B:7D:AC:12:21:08:4B:00:8C"
#define IFTTT_API_KEY "csdE5vbZ2AmtBqU65Jm6aA"

class IFTTTButton_1 : public Button{

public:
    IFTTTButton_1(uint8_t pin):Button(pin), wh(IFTTT_API_KEY, IFTTT_EVENT_NAME, IFTTT_FINGERPRINT){}
    void trigger();

protected:
    IFTTTWebhook wh;

};


#endif //SMARTBUTTON_IFTTTBUTTON_1_H
