//
// Created by Zhenyi Ye on 2019-09-05.
//
#ifndef SMARTBUTTON_POSTMANBUTTON_H
#define SMARTBUTTON_POSTMANBUTTON_H

#include <Arduino.h>
#include "InitFbConfig.h"
#include "NetworkUtil.h"
#include "Button.h"

class PostmanButton : public Button{

    public:
        PostmanButton(int pin, String answer): Button(pin){
            _answer = answer;
        }

        void trigger(){
            logDebug("Button Pressed : " + _answer);
            String s = String(BASE_URL);
            s += "?answer=" + _answer;
            String response = getRequest(s);
            if (response != "success")
            {
                logInfo("Failed to post feedback");
            }
        }

    private:
        String _answer;


};


#endif //SMARTBUTTON_POSTMANBUTTON_H
