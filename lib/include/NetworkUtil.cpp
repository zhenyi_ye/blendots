//
// Created by Zhenyi Ye on 2019-09-05.
//

#include "NetworkUtil.h"

// OAUTH2 Client credentials
String access_token = "";
String token_uri = "http://blendcx-security-dev.us-east-2.elasticbeanstalk.com:8080/oauth/token";
String client_id = "cyrana_client";
String client_secret = "admin";

void logDebug(String message){
    if(debug){
        Serial.println(message);
    }
}

void logInfo(String message){
    Serial.println(message);
}


void connectToWifi() {
    logInfo("Connecting to: SSID NAME " + String(ssid));
    if (WiFi.status() != WL_CONNECTED)
    {
        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, password);
        int i = 30;
        while(WiFi.status() != WL_CONNECTED && i >=0) {
            delay(1000);
            Serial.print(".");
            i--;
        }
    }
    Serial.println(" ");
    if(WiFi.status() == WL_CONNECTED){
        logInfo("WiFi Connected. IP address :" + WiFi.localIP().toString());

    }else {
        logInfo("Connection failed - check your credentials or connection");
    }
}

bool getOAUTH2Token(){
    String response = "";
    String postData = "";
    postData += "client_id=" + client_id;
    postData += "&client_secret=" + client_secret;
    postData += "&grant_type=client_credentials";

    logDebug(base64::encode(client_id +":" + client_secret));
    String authHeader = "Basic " + base64::encode(client_id +":" + client_secret);
    response = postRequest(token_uri, authHeader, postData);

    const size_t bufferSize = response.length();
    DynamicJsonDocument jsonDoc(bufferSize);
    deserializeJson(jsonDoc, response);
    if (jsonDoc.containsKey("access_token")){
        const char* at = jsonDoc["access_token"];
        access_token = String(at);
    }
    else{
        access_token = "";
        return false;
    }

    return true;
    logDebug(access_token);
}

String postRequest(String url, String authheader, String data) {
    String response = "";
    logInfo(String(WiFi.status()));
    logDebug(String(WL_CONNECTED));
    //connectToWifi();
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http; //Object of class HTTPClient
        http.begin(url);
        http.addHeader("Authorization", authheader);
        http.addHeader("Content-Type", "application/x-www-form-urlencoded");
        int httpCode = http.POST(data);
        logInfo(String(httpCode));
        if (httpCode > 0)
        {
            response = http.getString();
        }
        http.end();
    }else{
        logInfo("Error Connecting to WIFI");
    }
    logDebug(response);
    return response;
}

String getRequest(String url){
    String response = "";
    //connectToWifi();
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        //url += "&terminal=" + WiFi.macAddress();
        url += "&terminal=" + TEST_MAC_ID;
        logDebug(url);
        http.begin(url);
        if(access_token == ""){
            bool success = getOAUTH2Token();
            if(!success){
                return "Fail";
            }
        }
        http.addHeader("Authorization", "Bearer " + access_token);
        http.addHeader("Content-Type", "text/plain");

        int httpCode = http.GET();

        if (httpCode > 0)
        {
            response = http.getString();
        }
        http.end();
    }else{
        logInfo("Error Connecting to WIFI");
    }
    logDebug(response);
    return response;
}
