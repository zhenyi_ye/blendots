//
// Created by Zhenyi Ye on 2019-08-07.
//

#ifndef SMARTBUTTON_CONSTANT_H
#define SMARTBUTTON_CONSTANTS_H

#define TASK_RESET   0x00
#define TASK_SERVICE 0x01

const uint8_t LED_CLK = 16;
const uint8_t LED_DATA = 0;
const uint8_t LED_LATCH = 15;

#endif //SMARTBUTTON_CONSTANT_H
