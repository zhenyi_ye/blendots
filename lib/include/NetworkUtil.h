#ifndef SMARTBUTTON_NETWORKUTIL_H
#define SMARTBUTTON_NETWORKUTIL_H


#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <base64.h>
#include <ESP8266WiFi.h>
#include "InitFbConfig.h"

#define debug true
//#define ledPin 5
//#define wakePin 16

const String ssid = "Eklavya";
const String password = "22056DilworthSq";
const String BASE_URL = "http://blendcx-collector-dev.us-east-2.elasticbeanstalk.com:8080/collect/terminal/response";
const String TEST_MAC_ID = "68:C6:3A:D6:F7:BD";


String postRequest(String url, String authheader, String data);
void connectToWifi();
String getRequest(String url);
void logDebug(String message);
void logInfo(String message);

#endif