//
// Created by Zhenyi Ye on 2019-08-07.
//

#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include <FS.h>
#include <Ticker.h>
#include "ButtonFactory.h"

// Required for LIGHT_SLEEP_T delay mode
extern "C" {
#include "user_interface.h"
#include "gpio.h"
}

#include <ArduinoJson.h>
#include "IFTTTWebhook.h"
#include "PostmanButton.h"

// bool value for change config file
bool shouldSaveConfig;
String last_ssid;
String last_pwd;

// buffer for task
uint8_t next_task = 255;

// timer count
boolean wake_up = false;
boolean sleep = false;

// function headers
void wifi_init();
void wifi_reconnect();
void saveConfigCallback();
void task_reset();
void sleep_callback();
void light_sleep();

// buttons
PostmanButton btn_pin4(4, "1");
PostmanButton btn_pin5(5, "2");
PostmanButton btn_pin12(12, "3");
PostmanButton btn_pin13(13, "4");
PostmanButton btn_pin14(14, "5");

// button factory
ButtonFactory btn_factory;

// sleep timer
Ticker sleep_ticker;

void setup(){

    Serial.begin(115200);
    Serial.print(ESP.getSdkVersion());
    wifi_init();
    gpio_init();

    // init buttons
    btn_pin4.begin();
    btn_pin5.begin();
    btn_pin12.begin();
    btn_pin13.begin();
    btn_pin14.begin();
    btn_factory.buttonRegister(&btn_pin4);
    btn_factory.buttonRegister(&btn_pin5);
    btn_factory.buttonRegister(&btn_pin12);
    btn_factory.buttonRegister(&btn_pin13);
    btn_factory.buttonRegister(&btn_pin14);
    btn_factory.btnComboRegister(&btn_pin4, &btn_pin5, task_reset);

    // Start Sleep
    delay(200);
    light_sleep();
}

void loop(){

    // Wake Up
    if (sleep){
        light_sleep();
    }

    // Reset Timer If Buttons are Activated
    if (btn_factory.loop()){
        sleep_ticker.once(300, sleep_callback);
    }

}

void saveConfigCallback(){
    shouldSaveConfig = true;
}

// WiFi initialization
void wifi_init(){

    /* Read default config */
    if (SPIFFS.begin()) {
        Serial.println("mounted file system");
        if (SPIFFS.exists("/config.json")) {
            //file exists, reading and loading
            Serial.println("reading config file");
            File configFile = SPIFFS.open("/config.json", "r");
            if (configFile) {
                Serial.println("opened config file");
                size_t size = configFile.size();
                if (size > 4){
                    // Allocate a buffer to store contents of the file.
                    std::unique_ptr<char[]> buf(new char[size]);
                    configFile.readBytes(buf.get(), size);
                    Serial.printf("size: %d\n", size);
                    //DynamicJsonBuffer jsonBuffer;
                    //JsonObject& json = jsonBuffer.parseObject(buf.get());
                    StaticJsonDocument<512> doc;
                    DeserializationError error = deserializeJson(doc, configFile);
                    if (error){
                        Serial.println("Failed to read file, using default configuration");
                        configFile.close();
                        return;
                    }

                    serializeJson(doc, Serial);
                    //if (doc.success()) {
                        Serial.println("\nparsed json");

                        //strcpy(octo_server, json["octo_server"]);
                        //strcpy(octo_port, json["octo_port"]);
                        //strcpy(octo_token, json["octo_token"]);

                    //} else {
                      //  Serial.println("failed to load json config");
                    //}
                    configFile.close();
                } else
                    configFile.close();
            }
        }
    } else {
        Serial.println("failed to mount FS");
    }

    WiFiManager wifiManager;
    wifiManager.setSaveConfigCallback(saveConfigCallback);


    /* Set up Wifi */
    WiFi.setAutoConnect(true);
    wifiManager.autoConnect("AutoConnectAP");
    Serial.println("wifi connected!");
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
    Serial.print("MAC: ");
    Serial.println(WiFi.macAddress());
    last_ssid = WiFi.SSID();
    last_pwd = WiFi.psk();

    if(shouldSaveConfig){
        Serial.println("saving config");
        //DynamicJsonBuffer jsonBuffer;
        //JsonObject& json = jsonBuffer.createObject();
        StaticJsonDocument<512> doc;
        //json["octo_server"] = octo_server;
        //json["octo_port"] = octo_port;
        //json["octo_token"] = octo_token;

        File configFile = SPIFFS.open("/config.json", "w");
        if (!configFile) {
            Serial.println("failed to open config file for writing");
        }

        serializeJson(doc, Serial);
        if (serializeJson(doc, configFile) == 0) {
            Serial.println("Failed to write to file");
        }
        configFile.close();
        //end save
    }

}

void wifi_reconnect(){
    //WiFiManager wifiManager;
    //wifiManager.autoConnect("AutoConnectAP");
    WiFi.mode(WIFI_STA);
    delay(100);
    WiFi.forceSleepWake();
    WiFi.begin(last_ssid, last_pwd);
    while (WiFi.status() == WL_DISCONNECTED) {
        Serial.write('.');
        delay(500);
    }
    Serial.println("wifi connected!");
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
    delay(200);
}

void sleep_callback(){
    sleep = true;
}

void task_reset(){
    // blink led
    pinMode(LED_CLK, OUTPUT);
    pinMode(LED_DATA, OUTPUT);
    pinMode(LED_LATCH, OUTPUT);
    for (int i = 0; i < 3; i++){
        digitalWrite(LED_LATCH, LOW);
        shiftOut(LED_DATA, LED_CLK, MSBFIRST, 0);
        digitalWrite(LED_LATCH, HIGH);
        delay(500);
        digitalWrite(LED_LATCH, LOW);
        shiftOut(LED_DATA, LED_CLK, MSBFIRST, 0xFF);
        digitalWrite(LED_LATCH, HIGH);
        delay(500);
    }

    Serial.println("reset!");
    WiFiManager wifiManager;
    wifiManager.startConfigPortal("AutoConnectAP");

    // blink led
    pinMode(LED_CLK, OUTPUT);
    pinMode(LED_DATA, OUTPUT);
    pinMode(LED_LATCH, OUTPUT);
    for (int i = 0; i < 2; i++){
        digitalWrite(LED_LATCH, LOW);
        shiftOut(LED_DATA, LED_CLK, MSBFIRST, 0);
        digitalWrite(LED_LATCH, HIGH);
        delay(200);
        digitalWrite(LED_LATCH, LOW);
        shiftOut(LED_DATA, LED_CLK, MSBFIRST, 0xFF);
        digitalWrite(LED_LATCH, HIGH);
        delay(200);
    }

    // turn off led
    shiftOut(LED_DATA, LED_CLK, MSBFIRST, 0);
}

// Light Sleep
void light_sleep(){
    //wifi_station_disconnect();
    //wifi_set_opmode_current(NULL_MODE);
    Serial.println("Going to sleep now");
    delay(200);
    WiFi.persistent(false);
    WiFi.disconnect();
    WiFi.persistent(true);
    WiFi.mode(WIFI_OFF);
    delay(100);  // FIXME
    wifi_set_opmode_current(WIFI_OFF);
    wifi_fpm_set_sleep_type(LIGHT_SLEEP_T); // set sleep type, the above    posters wifi_set_sleep_type() didnt seem to work for me although it did let me compile and upload with no errors
    gpio_pin_wakeup_enable(GPIO_ID_PIN(2), GPIO_PIN_INTR_LOLEVEL); // GPIO_ID_PIN(2) corresponds to GPIO2 on ESP8266-01 , GPIO_PIN_INTR_LOLEVEL for a logic low, can also do other interrupts, see gpio.h above
    wifi_fpm_open(); // Enables force sleep
    wifi_fpm_do_sleep(0xFFFFFFF); // Sleep for longest possible time
    delay(100);  // FIXME
    sleep = false;
    sleep_ticker.once(300, sleep_callback);
}